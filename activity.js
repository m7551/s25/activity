
//Add the following documents in the new fruits collection with the following fields and values:

db.fruits.insertMany([
{
	"name": "Banana",
	"supplier": "Farmer Fruits Inc.",
	"stocks": 30,
	"price": 20,
	"onSale": true
},
{
	"name": "Mango",
	"supplier": "Mango Magic Inc.",
	"stocks": 50,
	"price": 70,
	"onSale": true
},
{
	"name": "Dragon Fruit",
	"supplier": "Farmer Fruits Inc.",
	"stocks": 10,
	"price": 60,
	"onSale": true
},
{
	"name": "Grapes",
	"supplier": "Fruity Co.",
	"stocks": 30,
	"price": 100,
	"onSale": true
},
{
	"name": "Apple",
	"supplier": "Apple Valley",
	"stocks": 0,
	"price": 20,
	"onSale": false
},
{
	"name": "Papaya",
	"supplier": "Fruity Co.",
	"stocks": 15,
	"price": 60,
	"onSale": true
}
])


//use count operator to count the total number of fruits on sale
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$count: "TotalNumberOfFruitsOnSale"}

])


//Use count operator to count the total numbers of fruits with stock >20
db.fruits.aggregate([
	{$match: {"stocks": {$gt: 20} }},
	{$count: "fruitStock"}
])

//Use average operator to get average price on sale per supplier
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplier", averagePrice: {$avg:"$price"}}}
])

//Use max operator to get the highest price of fruit per supplier
db.fruits.aggregate([
	{$group: {_id:"$supplier", maxPrice: {$max:"$price"}}}
])

//Use min operator to get the lowest price of fruit per supplier
db.fruits.aggregate([
	{$group: {_id:"$supplier", minPrice: {$min:"$price"}}}
])